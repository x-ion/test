<?php
namespace App\Models;

class ActionsModel extends Model  {

    protected $fields = ['title', 'content', 'email', 'images'];
    private $requestsPerDay = 3;

    function submitRequest() {
        $this->collectRequestData();
        if(!$this->validateRequest()) { return $this->result; }
        $this->object['ip'] = \Request::ip();
        if($this->countDailyRequestsByIp() >= $this->requestsPerDay) {
            $this->result->display = 'Максимальное количество заявок в день с одного айпи адреса не может превышать '.$this->requestsPerDay;
            return $this->result;
        }
        $this->result->result = $this->toTable();
        if(!$this->result->result) { return $this->result; }
        $this->result->display = 'Ваша заявка отправлена';
        return $this->result;
    }

    function countDailyRequestsByIp() {
        $today = date('Y-m-d') . ' 00:00:00';
        $tomorrow = date('Y-m').'-'.(date('d')+1). ' 00:00:00';
        return \DB::table('requests')
        ->where('ip', '=', \Request::ip())
        ->where('created_at', '>', $today)
        ->where('created_at', '<', $tomorrow)
        ->count();
    }

    function clearMyRequests() {
        $this->result->result = 1;
        $rows = \DB::table('requests')->where('ip', \Request::ip())->delete();
        $this->result->display = $rows ? 'Ваши заявки удалены' : 'У вас нет заявок';
        return $this->result;
    }

    function validateRequest() {
        $this->object['title'] = strip_tags($this->object['title']);
        $this->object['content'] = strip_tags($this->object['content']);
        if(!$this->object['title']) { $this->result->display = 'Пожалуйста укажите заголовок'; return 0; }
        if(mb_strlen($this->object['title']) < 5) { $this->result->display = 'Заголовок слишком короткий'; return 0; }
        if(mb_strlen($this->object['content']) < 5) { $this->result->display = 'Вопрос слишком короткий'; return 0; }
        if(!$this->object['content']) { $this->result->display = 'Пожалуйста введите текст вопроса'; return 0; }
        if(!$this->object['email']) { $this->result->display = 'Пожалуйста введите емайл'; return 0; }
        if(!filter_var($this->object['email'], FILTER_VALIDATE_EMAIL)) { $this->result->display = 'Емайл введен не корректно'; return 0; }
        $es = strlen($this->object['email']);
        if($es > 35) { $this->result->display = 'Емайл должен быть не длиннее 35 символов'; return 0; }
        $this->validateImages();
        return 1;
    }

    function validateImages() {
        $this->object['images'] = (array)$this->object['images'];
        if(empty($this->object['images'])) { return 1; }
        foreach($this->object['images'] as $k => $v) {
            if(!is_file('uploads/'.$v)) { unset($this->object[$k]); }
        }
        $this->object['images'] = \serialize($this->object['images']);
        return 1;
    }

    function toTable() {
        try {
            $this->result->result = \DB::table('requests')->insert($this->object);
        } catch(\PDOException $e) {
            $this->result->display = 'Извените произошла неизвестная ошибка'.$e->getMessage();
            return 0;
        }
        return 1;
    }


}