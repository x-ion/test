<?php
namespace App\Models;
use Intervention\Image\Facades\Image;

class FilesModel {
    public $image;
    public $imageName;
    public $imageExtension;
    public $imageSize;
    public $finalImageName;
    public $result;
    public $attempts = 0;
    public $hashname;
    public $compressionType = 0; // 0 horizontal
    public $compressionValue = 300;
    public $maxImageSize = 15; // mb
    public $rootDirectory = 'uploads';
    public $error;
    public $finalName;

    function __construct() {
        $this->result = new \Result;
    }


    function uploadImage() {
        if(!$this->getRequestData()) { return $this->result; }
        if(!$this->saveImage()) { return $this->result; }
        $this->result->result = 1;
        $this->result->name = $this->finalName;
        $this->result->link = \URL::asset($this->rootDirectory.'/'.$this->finalName);
        return $this->result;
    }

    function removeImage() {
        $this->image = \Request::get('image');
        @unlink($this->rootDirectory.'/'.$this->image);
        $result = new \Result;
        $result->result = 1;
        return $result;
    }




    function saveImage() {

        if($this->attempts > 150) {
            $this->result->display = 'Извените, не удалось сохранить изображение';
            return 0;
        }
        $filename = $this->attempts ? md5($this->attempts.$this->imageName).$this->attempts : $this->hashname;
        $this->finalName = $filename.'.jpg';
        $finalPath = $this->rootDirectory.'/'.$this->finalName;
        if(is_file($finalPath)) {
            $this->attempts++;
            return $this->saveImage();
        }
        try {
            $move = $this->image->move($this->rootDirectory, $this->finalName);
        } catch(\Exception $e) {
            $this->attempts++;
            $this->error = $e->getMessage();
            return $this->saveImage();
        }
        if(!$move) {
            $this->attempts++;
            return $this->saveImage();
        }
        $this->finalImageName = $this->finalName;
        return 1;
    }

    function getRequestData() {
        $this->image = \Request::file('image');
        if(!$this->image) { $this->result->display = 'Нет данных об изображении'; return 0; }
        $this->imageName = $this->image->getClientOriginalName();
        $this->imageExtension = $this->image->getClientOriginalExtension();
        $this->imageSize = $this->image->getClientSize();
        $this->hashname = md5($this->imageName);
        return 1;
    }
}