<?php
namespace App\Models;

class RequestsModel extends Model  {

    function getRequests() {
        return \DB::table('requests')->orderBy('created_at', 'desc')->get();
    }
}