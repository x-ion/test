<?php

namespace App\Models;

class Model {

    protected $fields;
    protected $object;
    protected $table;
    public $result;

    function __construct() {
        $this->result = new \Result;
    }

    function collectRequestData() {
        if(empty($this->fields)) { return; }
        foreach($this->fields as $x) {
            $this->object[$x] = trim(\Request::get($x));
        }
    }

}