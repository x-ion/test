<?php

namespace App\Http\Controllers;
use App\Models\ActionsModel;


class ActionsController extends Controller {

    protected $model;

    function __construct() {
        $this->model = new ActionsModel;
    }

    function submitRequest() {
        return \json_encode($this->model->submitRequest());
    }

    function clearMyRequests() {
        return \json_encode($this->model->clearMyRequests());
    }
}
