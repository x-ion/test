<?php

namespace App\Http\Controllers;
use App\Models\FilesModel;


class FilesController extends Controller {

    public $model;

    function __construct() {
        $this->model = new FilesModel;
    }

    function uploadImage() {
        return \json_encode($this->model->uploadImage());
    }
}
