<?php

namespace App\Http\Controllers;
use App\Models\RequestsModel;


class BackController extends Controller {

    public $requestsModel;

    function __construct() {
        $this->requestsModel = new RequestsModel;
    }

    function index() {
        $requests = $this->requestsModel->getRequests();
        return view('admin.pages.index', compact('requests'));
    }
}
