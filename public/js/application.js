$(function() {
    Actions.init();
    csrf(csrf_token);
});

var Actions = {
    currentWorkingImage: 0,
    imagePrototype: null,
    imagesList: null,
    image: null,
    init: function() {
        this.imagePrototype = $('#imagePrototype');
        this.imagesList = $('#imagesList');
        this.image = $('#image');
    },
    collectImages: function() {
        var a = [];
        this.imagesList.find('.image-item').each(function() {
            a.push($(this).find('img').attr('data-name'));
        });
        return a;
    },
    clearImages: function() {
        this.imagesList.find('.image-item').remove();
    },
    submit: function(el) {
    var formData = new FormData(el);
    formData.append('images', this.collectImages());
        $.ajax({
            url: el.action,
            type: 'post',
            data: formData,
            dataType: 'json',
            processData: false,
            contentType: false,
            cache: false,
            success: function(db) {
                if(db.result) {
                    el.reset();
                    Actions.clearImages();
                } else {

                }
                Actions.msg(db.display, 4000);
            }
        });
        return false;
    },
    msg: function(a, b) {
        $.toast({
            text: a,
            position: 'top-right',
            hideAfter: b ? b : 3000
        });
    },
    removeImage: function(el) {
        el.remove();
    },
    addImage: function(e) {
            this.image.trigger('click');
    },
    clearMyRequests: function() {
        $.ajax({
            type: 'post',
            url: '/clear-my-requests',
            dataType: 'json',
            success: function(db) {
                Actions.msg(db.display);
            }
        });
    },
    placeImageElement: function(link, name) {
        var i = this.currentWorkingImage.find('img');
        i.attr('src', link+'?'+ new Date().getTime());
        i.attr('data-name', name);
        this.currentWorkingImage.removeClass('blocked');
    },
    uploadImage: function(e) {
    if(this.imagesList.hasClass('block')) { return; }
    this.imagesList.addClass('block');
    var formData = new FormData($('#imageUploadForm')[0]);
    if(formData.get('image').size == 0) {
        this.imagesList.removeClass('block');
        return false;
    }
    $.ajax({
        url: '/images/upload',
        data: formData,
        type: 'POST',
        cache: false,
        dataType: 'json',
        processData: false,
        contentType: false,
        beforeSend: function() {
            Actions.currentWorkingImage = Actions.imagePrototype.clone();
            Actions.currentWorkingImage.attr('id', '');
            Actions.imagesList.append(Actions.currentWorkingImage);
        },
        success: function(db) {
            if(db.result) {
                Actions.placeImageElement(db.link, db.name);
            }
            Actions.imagesList.removeClass('block');
            Actions.currentWorkingImage = null;
        },
        error: function(a) {
            Actions.imagesList.removeClass('block');
            Actions.currentWorkingImage.remove();
            this.currentWorkingImage = null;
        }
    });

}
}

function csrf(token) {
    csrf_token = token;
    $.ajaxSetup({
        headers: { 'X-CSRF-TOKEN': token },
        type: 'post'
    });
}

