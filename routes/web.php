<?php

Route::get('/', 'FrontController@index');
Route::post('/submit-request', 'ActionsController@submitRequest');
Route::post('/clear-my-requests', 'ActionsController@clearMyRequests');
Route::post('/images/upload', 'FilesController@uploadImage');
Route::get('/admin', 'BackController@index');