@extends('layouts.front')

@section('page')
    <div class="col-xs-12">
        <form action="/submit-request" class="form-horizontal" onsubmit="return Actions.submit(this)">
            <div class="form-group">
                <label class="control-label col-sm-3">Название</label>
                <div class="col-sm-5">
                <input type="text" class="form-control" name="title" />
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-3">Емайл</label>
                <div class="col-sm-5">
                <input type="text" class="form-control" name="email" />
            </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-3">Описание</label>
                <div class="col-sm-5">
                <textarea name="content" class="form-control"></textarea>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-3">Изображения</label>
                <div class="col-sm-5">
                    <div class="images" id="imagesList">
                        <div class="image" id="addImage" onclick="Actions.addImage(event)">
                            <i class="mdi mdi-shape-circle-plus"></i>
                        </div>
                </div>
                </div>
                </div>
            <div class="form-group">
                <div class="col-sm-5 col-sm-offset-3">
                <button type="submit" class="btn btn-default">Отправить</button>
                <button type="button" class="btn btn-warning" onclick="Actions.clearMyRequests()">Очистить заявки с моего ip</button>
                </div>
            </div>
        </form>
    </div>
    <div id="prototypes">
        @include('elements.imagePrototype')
    </div>
    @include('elements.uploadForm', ['onchange' => 'onchange="Actions.uploadImage(event)"'])
@stop