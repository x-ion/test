@php $image = @$image ? $image : 0; @endphp
<div class="image image-item"@if(!$image) id="imagePrototype" @endif>
    <div class="actions">
        <button type="button" class="delete button-clear" onclick="Actions.removeImage(this)"><i class="fa fa-close round-close"></i></button>
    </div>
    <img src="{{ '/uploads/'.$image }}" onerror="this.src='/images/noimage.jpg'"  @if($image) data-name="{{ $image }}" @endif />
    @if(!$image)
        @include('elements.asyncPreloader', ['spinnerClass' => 'spinner-blue-only'])
    @endif
</div>