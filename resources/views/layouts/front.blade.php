@extends('layouts.app')

@section('body')
    <div class="container">
        <div class="row">
            @yield('page')
        </div>
    </div>
@stop