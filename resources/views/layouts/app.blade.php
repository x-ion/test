<!DOCTYPE Html>
<html>
<head>
<title>Test</title>
    <link rel="stylesheet" href="/css/materialdesignicons.min.css">
    <link rel="stylesheet" href="/css/bootstrap.min.css">
    <link rel="stylesheet" href="/css/app.css">
    <script src="/js/jquery.min.js"></script>
    <script src="/js/jquery.toast.js"></script>
    <script src="/js/bootstrap.min.js"></script>
    <script src="/js/application.js"></script>
@yield('head')
</head>
<body>
    @yield('body')
    <script>
        var csrf_token = '<?php echo csrf_token(); ?>';
    </script>
</body>
</html>