@extends('layouts.front')

@section('page')
    @foreach($requests as $x)
        <div class="col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading clearfix">
                    <div class="row">
                        <span class="col-xs-6">{!! $x->title !!}</span>
                        <span class="col-xs-6 text-right">от {!! $x->ip !!} в {!! $x->created_at !!}</span>
                    </div>
                </div>
                <div class="panel-body">{!! $x->content !!}</div>
                <div class="panel-footer clearfix">
                    @php $x->images = \unserialize($x->images); @endphp
                    @if(!empty($x->images))
                        @foreach($x->images as $image)
                            @include('elements.imagePrototype')
                        @endforeach
                    @endif
                </div>
            </div>
        </div>
    @endforeach
@stop